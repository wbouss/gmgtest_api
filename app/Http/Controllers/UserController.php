<?php

namespace App\Http\Controllers;

use App\Models\UserGmg;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \App\Http\Resources\UserGmg as UserResources;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = UserGmg::all();
        return UserResources::collection($users);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = UserGmg::create([
            'first_name' => $request->first_name,
            'name' => $request->name,
            'email' => $request->email
        ]);
        return new UserResources($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserGmg  $userGmg
     * @return \Illuminate\Http\Response
     */
        public function show(UserGmg $user)
    {
        $tasks = $user->tasks;
        return new UserResources($user);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserGmg  $userGmg
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserGmg $user)
    {
        $e = $user->update($request->only(['name', 'first_name', 'email']));
        return new UserResources($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserGmg  $userGmg
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserGmg $user)
    {
        $user->delete();
        return response()->json(null, 204);

    }
}
