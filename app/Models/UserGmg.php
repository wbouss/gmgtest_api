<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserGmg extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'name',
        'first_name',
        'email',
    ];

    /**
     * The attributes that are mass assignable.
     */
    protected $guarded = [
        'task',
    ];

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = "users_gmg";

    /**
     * Get the tasks for the user.
     */
    public function tasks(){
        return $this->hasMany(Task::class);
    }

}
