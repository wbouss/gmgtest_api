<?php

namespace Database\Seeders;

use App\Models\UserGmg;
use Illuminate\Database\Seeder;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserGmg::factory()->count(3)->create();
    }
}
