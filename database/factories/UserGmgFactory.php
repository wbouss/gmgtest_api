<?php

namespace Database\Factories;

use App\Models\UserGmg;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserGmgFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserGmg::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'first_name' => $this->faker->firstName,
            'email' => $this->faker->unique()->safeEmail,
        ];
    }
}
